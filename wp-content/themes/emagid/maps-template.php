<?php
/**
 * Template Name: Maps Template
 *
 */
get_header();
?>
  <section class="inner_template">     
      
<div class="nav_bar" id="sticker">
    <?php get_sidebar('location'); ?>

</div>


    <section class="section_three_home">
        <div class="content">
            <h2><?php the_field('overview_title'); ?> Directions</h2>
            <?php the_field('overview'); ?>
        </div>
        
    </section>
    
      <section class="service_flex">
          <div class="wrapper">
            <div class="flex" id="website">
                <?php the_field('google_map'); ?>
            </div>
          </div>
        </section>
      
    <section class="section_three_home">
        <div class="content">
            <?php the_field('transportation'); ?>
        </div>
        
    </section>


</section> 

<!--
<style>
    .site_holder header {
        position: relative;
    }
    .site_content {
        padding-top: 0
    }
</style>
-->

<script>
  $(document).ready(function(){
    $("#sticker").sticky({topSpacing:0});
  });
</script>

<?php
get_footer();
