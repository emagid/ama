<?php

get_header();
?>


    <section class="section_six_home" style="background-image:url(<?php the_field('banner_image'); ?>)" >
        <div class="content get_started">
            
            <div class="get_started_header">
                <h5><?php the_field('title'); ?></h5>
                <h4><?php the_field('subtext'); ?></h4>
            </div>
            
        </div>
        
        <section>
    <div class="our_services_section pricing contacts" id="rates">
        <div class="wrapper">

            
            <section class="section_five_home" id="hotels">
                <h2>New York</h2>
            <div class="content">

    <?php

// New York

                    
                $args = array(
        'post_type' => 'contacts',
                    'cat' => '7'
        ); 
                
                
        $products = new WP_Query( $args );
            if( $products->have_posts() ) {
            while( $products->have_posts() ) {
        $products->the_post();
    ?> 
                <div class="media_box" id="contact_card">

                    <div class="header contact_header">
                        <div class="wrapper">
                            <h5><?php the_field('name'); ?></h5>
                            <p><?php the_field('contact_info'); ?></p>
                        </div>
                    </div>     
                </div>
    <?php
        }
            }
        else {
        echo 'No Contacts Found';
        }
    ?> 
         
                


            
        </div>
</section>
            
   
      
            <section class="section_five_home" id="hotels">
            <h2>Atlanta</h2>   
            <div class="content">

                                  <?php

//                Atlanta
                    
                $args = array(
        'post_type' => 'contacts',
                    'cat' => '8'
        ); 
                
                
        $products = new WP_Query( $args );
            if( $products->have_posts() ) {
            while( $products->have_posts() ) {
        $products->the_post();
    ?> 
                <div class="media_box" id="contact_card">

                    <div class="header contact_header">
                        <div class="wrapper">
                            <h5><?php the_field('name'); ?></h5>
                            <p><?php the_field('contact_info'); ?></p>
                        </div>
                    </div>     
                </div>
    <?php
        }
            }
        else {
        echo 'No Contacts Found';
        }
    ?> 
         
                


            
        </div>
</section>
            
            
      
<section class="section_five_home" id="hotels">
    <h2>San Francisco</h2>
            <div class="content">

<?php
//                San Francisco
                    
                $args = array(
        'post_type' => 'contacts',
                    'cat' => '9'
        ); 

                
                
        $products = new WP_Query( $args );
            if( $products->have_posts() ) {
            while( $products->have_posts() ) {
        $products->the_post();
    ?> 
                <div class="media_box" id="contact_card">

                    <div class="header contact_header">
                        <div class="wrapper">
                            <h5><?php the_field('name'); ?></h5>
                            <p><?php the_field('contact_info'); ?></p>
                        </div>
                    </div>     
                </div>
    <?php
        }
            }
        else {
        echo 'No Contacts Found';
        }
    ?> 
         
                


            
        </div>
</section>
            
            
      
<section class="section_five_home" id="hotels">
    <h2>Washington, D.C.</h2>
            <div class="content">

                                  <?php
//                Washington DC
        $args = array(
        'post_type' => 'contacts',
            'cat' => '10'
            
        );
                
                
                
        $products = new WP_Query( $args );
            if( $products->have_posts() ) {
            while( $products->have_posts() ) {
        $products->the_post();
    ?> 
                <div class="media_box" id="contact_card">

                    <div class="header contact_header">
                        <div class="wrapper">
                            <h5><?php the_field('name'); ?></h5>
                            <p><?php the_field('contact_info'); ?></p>
                        </div>
                    </div>     
                </div>
    <?php
        }
            }
        else {
        echo 'No Contacts Found';
        }
    ?> 
         
                


            
        </div>
</section>
            
            
    </div>
    </div>
</section>
        <div class="form_container">
            
            <div class="form_apply form_get_started">
                <h5>WANT TO CHAT ABOUT MEETING SPACE?</h5>
                <h2>Leave a Message using the the form below.</h2>
                <?php echo do_shortcode('[contact-form-7 id="85" title="Get Started Form"]'); ?>
                
            </div>
        </div>
    </section>



<?php
get_footer();
