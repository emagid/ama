<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header();
?>
        
    <div class="hero section" id="splash" style="background-image:url(<?php the_field('banner_image'); ?>);">
        <div class="wrapper">
                        <div class="two_sided">
                <div class="grey_side" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/grey_banner.png);">
                </div>
                
                <div class="orange_side">
                    
                </div>
                </div>
        <div class="hero_brand">
            <div class="wrapper">
                <div>
                    <h2 id="brand_trigger" class="active"><?php the_field('banner_title'); ?></h2>

                    <div class="copy">
                        <p><?php the_field('banner_text'); ?></p>
                        <div class="cta">
                            <a href="/book-now/step-1/">
                                <button>Book Now</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      

        </div>
    
</div>



    <section class="section_three_home">
        <div class="content">
                    
            <h2><?php the_field('intro_header'); ?></h2>
            <?php the_field('intro_text'); ?>
            <div class="comment">
                <?php the_field('comment'); ?>
                </div>
        </div>
            <section class="section_five_home" id="press">
        <div class="content">
            
            
                                  <?php
        $args = array(
        'post_type' => 'locations'
        );
        $products = new WP_Query( $args );
            if( $products->have_posts() ) {
            while( $products->have_posts() ) {
        $products->the_post();
    ?> 

            
                <div class="media_box" style="background-image:url(<?php the_field('image'); ?>)">
                    <a href="<?php the_field('link'); ?>">
                    <div class="overlay_amenities">
                        <div class="wrapper">
                            <h5>Amenties Include:</h5>
                            <ul>
                                <li>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery.png">
                                </li>
                                <li>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/package.png">
                                </li>
                                <li>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/hotel.png">
                                </li>
                                                                <li>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/explore.png">
                                </li>
                                <li>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/menu.png">
                                </li>
                            </ul>
                        </div>
                    </div>
                        </a>
                    <div class="header">
                        <div class="wrapper">
                            <h5><?php the_field('location'); ?></h5>
                            <p><?php the_field('rooms_available'); ?> Rooms available</p>
                        </div>
                    </div>
                </div>
            
            <?php
        }
            }
        else {
        echo 'No Locations Found';
        }
    ?>     

            </div>
        
        
    </section>
        
    </section>
<section>
    <div class="our_services_section">

        <div class="wrapper">
            <h2>AMA's Executive Conference Centers offer:</h2>
            
            <div class="service_circles">
                <div class="img_contain">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_1.png" alt="">
                </div>
                <p>flexible configurations <br>for up to <span>240 people</span></p>
            </div>
            <div class="service_circles">
                <div class="img_contain">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_2.png" alt="">
                </div>
                <p>No food and <br> beverage <span>required</span></p>
            </div>
                        <div class="service_circles">
                <div class="img_contain">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_3.png" alt="">
                </div>
                <p><span>Complimentary</span> <br>Beverage Service</p>
            </div>
            <div class="service_circles">
                <div class="img_contain">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_4.png" alt="">
                </div>
                <p><span>Free</span> LCD and PC use</p>
            </div>
            <div class="service_circles">
                <div class="img_contain">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_5.png" alt="">
                </div>
                <p><span>Executive</span> chairs </p>
            </div>
            <div class="service_circles">
                <div class="img_contain">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_6.png" alt="">
                </div>

                <p><span>No</span> Service Charges</p>
            </div>
            
        </div>
    </div>

</section>

    <div class="hero section section_six_home" id="contact_section">
    <section class="section_touch " style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/work.png);">
        <div class="content">
            <h6>Want to Book A Room</h6>
            <p>Reach Out, Let's Chat</p>
            <div class="cta">
                <a href="#">
                    <button>Contact</button>
                </a>
            </div>
        </div>
        

    </section>
        <div class="form_container">
            <div class="form_apply form_get_started">
                <?php echo do_shortcode('[contact-form-7 id="85" title="Contact Us""]'); ?>
                
            </div>
        </div>

    </div>



<section>
    <div class="our_services_section partners">
        <div class="wrapper">
            <h2>Our Partners</h2>
            
            <div class="partner_section">
                
                      <?php
        $args = array(
        'post_type' => 'partner',
        'posts_per_page' => 99
        );
        $products = new WP_Query( $args );
            if( $products->have_posts() ) {
            while( $products->have_posts() ) {
        $products->the_post();
    ?> 


                <div class="service_circles">
                    <div class="img_contain">
                        <img src="<?php the_field('logo'); ?>" alt="" class="<?php the_field('override'); ?>">
                    </div>
                </div>
                     
                
    <?php
        }
            }
        else {
        echo 'No Partners Found';
        }
    ?> 

            </div>
            
            <div class="testimonials">
                <h2>Testimonials</h2>
                <img class='quote' src="<?php echo get_template_directory_uri(); ?>/assets/img/quotes.png">
                <!-- <h2>Testimonials</h2> -->
                <!-- Slider main container -->
                <div class="swiper-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        
                                                          <?php
        $args = array(
        'post_type' => 'testimonials',
        'posts_per_page' => 99
        );
        $products = new WP_Query( $args );
            if( $products->have_posts() ) {
            while( $products->have_posts() ) {
        $products->the_post();
    ?> 
                        <div class="testament swiper-slide">
                            <h3><?php the_field('name'); ?></h3>
                            <?php the_field('testimonial'); ?>
                        </div>
                        
                        
                            <?php
        }
            }
        else {
        echo 'No Testimonials Found';
        }
    ?> 

                    </div>
                    <!-- If we need pagination -->
                    <div class="swiper-pagination"></div>
                </div>
                
            </div>
            
        </div>
    </div>
    

<!--
     <div class="our_services_section faqs">
        <div class="wrapper">
            <h2>Frequently Asked Questions</h2>
            
            <section class="section_five_home" id="press">
        <div class="content">

            <div class="media_box">
                <div class="faq">
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lob?</p>
                </div>   
            </div>
            <div class="media_box">
                <div class="faq">
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lob?</p>
                </div>   
            </div>
            <div class="media_box">
                <div class="faq">
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt?</p>
                </div>   
            </div>
            <div class="media_box">
                <div class="faq">
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt?</p>
                </div>   
            </div>
            <div class="cta">
                <a href="/book-now">
                    <button>View all</button>
                </a>
            </div>
            
            </div>      
                
        </section>
    </div>
    </div>
-->

</section>

<script>
  $(document).ready(function () {
    // Page opening animations
    setTimeout(function(){
        $('div.hero#splash .wrapper').css('width', '100%');
        $('#splash').addClass('move');
    }, 1000);

    setTimeout(function(){
        $('div.hero .hero_brand .wrapper > div').fadeIn();
    }, 1500);


    $(document).scroll(function(){
        var divs = $('.section_three_home, .faqs');
        
        divs.each(function(i){
        var div = $(divs)[i];
          var height = $(div).height();
          var top = $(div).offset().top;
          var w_height = $(window).scrollTop();
          if ( w_height >= top - 400 ) {
            $(div).css('background-color', '#f5f6f9');
          }
      });
    });


    $(document).scroll(function(){
        var divs = $('.media_box');

        divs.each(function(i){
        var div = $(divs)[i];
          var height = $(div).height();
          var top = $(div).offset().top;
          var w_height = $(window).scrollTop();
          if ( w_height >= top - 800 ) {
            $(div).css({'transform': 'translateY(0)', 'opacity': '1'}).fadeIn(500);
          }
        });
    });

    $(document).scroll(function(){
        var divs = $('.service_circles');

        divs.each(function(i){
        var div = $(divs)[i];
          var height = $(div).height();
          var top = $(div).offset().top;
          var w_height = $(window).scrollTop();
          if ( w_height >= top - 800 ) {
            $(div).css({'transform': 'translateY(0)', 'opacity': '1'}).fadeIn(500);
          }
        });
    });



    //initialize swiper when document ready
    var mySwiper = new Swiper ('.swiper-container', {
      // Optional parameters
      loop: true,
        
           // If we need pagination
    pagination: {
      el: '.swiper-pagination',
    },
        
          autoplay: {
    delay: 5000,
  }
        
    });
    
      
    $("ul.menu li.scroll").find("a").click(function(e) {
        e.preventDefault();
        var section = $(this).attr("href");
        $("html, body").animate({
            scrollTop: $(section).offset().top
        });
    });
      
    $( "div#contact_section .content .cta a" ).click(function(e) {
        e.preventDefault();
          $( "div#contact_section .form_container" ).toggle();
        });
      
  });
</script>

<?php
get_footer();
