<?php
/**
 * Template Name: Chicago Template
 *
 */
get_header();
?>
  <section class="inner_template">     
    <div class="hero section" id="splash" style="background-image:url(<?php the_field('banner'); ?>);">
        <div class="wrapper">
                        <div class="two_sided">
                <div class="grey_side" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/grey_banner.png);">
                
                    <div class="inner_hero_content">
                        <div class="wrapper">
                            <h2 class="active"><?php the_title(); ?></h2>

                            <div class="copy">
                                <p><?php the_field('address'); ?></p>
                            <div class="cta" id="ny_book">
                                <a href="/book-now/step-1/">
                                    <button>Book Now</button>
                                </a>
                            </div>
                            <div class="cta" id="atl_book">
                                <a href="/book-now/atl-step-1/">
                                    <button>Book Now</button>
                                </a>
                            </div>
                            <div class="cta" id="sf_book">
                                <a href="/book-now/sf-step-1/">
                                    <button>Book Now</button>
                                </a>
                            </div>
                            <div class="cta" id="dc_book">
                                <a href="/book-now/dc-step-1/">
                                    <button>Book Now</button>
                                </a>
                            </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
      

        </div>
    
</div>


    <section class="section_three_home" id="overview" style="width:100%;">
        <div class="content">
            <h2><?php the_field('overview_title'); ?></h2>
            <?php the_field('overview'); ?>
        </div>
        
    </section>
    

      



<style>
    .site_holder header {
        position: relative;
    }
    .site_content {
        padding-top: 0
    }
</style>

<script>
$(document).ready(function(){

    // Page opening animations
    setTimeout(function(){
        $('.grey_side').css('width', '65%');
    }, 500);

    setTimeout(function(){
        $('.inner_hero_content').fadeIn();
    }, 1000);

    $(".nav_bar").find("a.scroll").click(function(e) {
        e.preventDefault();
        var section = $(this).attr("href");
        $("html, body").animate({
            scrollTop: $(section).offset().top
        });
    });
    
    $("#sticker").sticky({topSpacing:0});
    
    
});
</script>

<?php
get_footer();
