<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>

	</div><!-- .site_content-->

	<footer class="site_footer" id="desktop">

        <div class="wrapper">
            <div class="column" id="footer_brand">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ama_footer.png" alt="logo_alt">
            </div>
                        <div class="column" id="footer_links">
                <h5>Location</h5>
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-2'
                    ) );
                    ?>
            </div>
            <div class="column" id="footer_links">
                <h5>About AMA</h5>
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-3'
                    ) );
                    ?>
            </div>

            <div class="column social" id="footer_links">
                <ul class="social">
                    
                                        <li>
                        <a href="http://amaconferencecentersspeak.com/" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/blogging.png" alt="Blog Logo">
                        </a>
                    </li>

                    <li>
                        <a href="https://www.facebook.com/pages/AMA-Executive-Conference-Centers/124541270540" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/facebook.png" alt="Facebook Logo">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.linkedin.com/company/ama-executive-conference-centers/?trk=biz-companies-cym" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/linkedin-logo.png" alt="Linkedin Logo">
                        </a>
                    </li>
                    
                    <li>
                        <a href="https://twitter.com/AMAConfCenters" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/twitter.png" alt="Twitter Logo">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/amaconferencecenter/" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/instagram.png" alt="Instagram Logo">
                        </a>
                    </li>
                                        <li>
                        <a href="https://www.pinterest.com/amaconfcenters" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/pinterest-logo.png" alt="pinterest Logo">
                        </a>
                    </li>
                                        

                </ul>
            </div>
            
        </div>
                <div class="bottom_bar">
            <p>Built for Meetings. Made for success. &nbsp; | &nbsp; <span>212-903-8277</span></p>
        </div>
        
	</footer>

</div><!-- .site_holder -->
<!--
<script>
    
    
$(window).load(function(){
   $(".hero_brand").show("slide", {
      direction: "up"
   }, 1500);
    $(".two_sided").show("slide", {
      direction: "right"
   }, 2000);
    
    
});
</script>
-->
    <script type="text/javascript">
var _userway_config = {
// uncomment the following line to override default position
 position: '3',
// uncomment the following line to override default size (values: small, large)
// size: 'small', 
// uncomment the following line to override default language (e.g., fr, de, es, he, nl, etc.)
// language: null,
// uncomment the following line to override color set via widget
// color: 'null', 
// uncomment the following line to override type set via widget(1=man, 2=chair, 3=eye)
// type: 'null', 
account: 'zGw3F0hD0t'
};
</script>
<script type="text/javascript" src="https://cdn.userway.org/widget.js"></script>

<?php wp_footer(); ?>

</body>
</html>
