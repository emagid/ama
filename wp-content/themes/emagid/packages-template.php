<?php
/**
 * Template Name: Packages Template
 *
 */
get_header();
?>
  <section class="inner_template">     

<div class="nav_bar" id="sticker">
    <?php get_sidebar('location'); ?>
</div>



    <section class="section_three_home" id="amenities">
        <div class="content">
            <h2>Meeting Packages and Rates</h2>
            <p><strong>Meeting Package includes:</strong></p>
            <?php the_field('amenities'); ?>
        </div>
        
    </section>
    
      
<section>

    <div class="our_services_section pricing alt_pricing" id="rates">
            <div class="package_header">
                <ul class="package_select">
                    <li><a href="#meeting">Meeting Room</a></li>
                    <li><a href="#conference">Conference Room</a></li>
                    <li><a href="#rentals">Audio/Visual Rentals</a></li>
                </ul>
            </div>
        
        <div class="meeting_package" id="meeting">
            <div class="wrapper">
                <h2>Meeting Room Package Prices</h2>
                <p style="width:80%;margin:0 auto;">Comprehensive meeting package includes audio visual stated above. Additional audio visual equipment can be rented at the following prices:</p>
                <section class="section_five_home" id="pricing">
                    <div class="content">
                        <div class="package_overview">
                            <?php the_field('packages'); ?>
                        </div>

                    </div>
                </section>
            </div>
        </div>
        
        <div class="conference_package" id="conference">
            <div class="wrapper">
                <h2>Conference Room & Project Room Prices</h2>
                                <p style="width:80%;margin:0 auto;">Comprehensive meeting package includes audio visual stated above. Additional audio visual equipment can be rented at the following prices:</p>
                <section class="section_five_home" id="pricing">
                    <div class="content">
                        <div class="package_overview">
                            <?php the_field('conference_packages'); ?>
                        </div>

                    </div>
                </section>
            </div>
        </div>
        
        <div class="rentals" id="rentals">
            <div class="wrapper">
                <h2>Audio Visual Rentals</h2>
                <p style="width:80%;margin:0 auto;">Comprehensive meeting package includes audio visual stated above. Additional audio visual equipment can be rented at the following prices:</p>
                <section class="section_five_home" id="pricing">
                    <div class="content">
                        <div class="package_overview">
                            <?php the_field('audio_visual_rentals'); ?>
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </div>

</section>

</section> 


<style>
    .site_holder header {
        position: relative;
    }
    .site_content {
        padding-top: 0
    }
</style>

<script>
$(document).ready(function(){

    $(".nav_bar").find("a.scroll").click(function(e) {
        e.preventDefault();
        var section = $(this).attr("href");
        $("html, body").animate({
            scrollTop: $(section).offset().top
        });
    });
    
    $("#sticker").sticky({topSpacing:0});
    
    $('a[href*=\\#]').on('click', function(e){
        e.preventDefault();
        $('html, body').animate({
            scrollTop : $(this.hash).offset().top - 150
        }, 500);
    });
    
//    $("li#meeting").click(function(){
//        $(".conference_package").hide();
//        $(".rentals").hide();
//        $(".meeting_package").show();
//        $("ul.package_select li").removeClass("active");
//        $(this).addClass("active");
//    });
//    
//    $("li#conference").click(function(){
//        $(".rentals").hide();
//        $(".meeting_package").hide();
//        $(".conference_package").show();
//        $("ul.package_select li").removeClass("active");
//        $(this).addClass("active");
//    });
//    
//    $("li#rentals").click(function(){
//        $(".conference_package").hide();
//        $(".meeting_package").hide();
//        $(".rentals").show();
//        $("ul.package_select li").removeClass("active");
//        $(this).addClass("active");
//    });
});
</script>

<?php
get_footer();
