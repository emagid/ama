<?php
/**
 * Template Name: Floor Plans Template
 *
 */
get_header();
?>
  <section class="inner_template">     
<!--
    <div class="hero section" id="splash" style="background-image:url(<?php the_field('banner'); ?>);">
        <div class="wrapper">
                        <div class="two_sided">
                <div class="grey_side" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/grey_banner.png);">
                
                    <div class="inner_hero_content">
                        <div class="wrapper">
                            <h2 class="active"><?php the_title(); ?></h2>

                            <div class="copy">
                                <p><?php the_field('address'); ?></p>
                                            <div class="cta">
                            <a href="">
                                <button>Book Now</button>
                            </a>
                        </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
      

        </div>
    
</div>
-->
<div class="nav_bar" id="sticker">
    <?php get_sidebar('location'); ?>
</div>


    <section class="section_three_home">
        <div class="content">
            <h2>Floor Plan and Room Capacities</h2>
            <?php the_field('overview'); ?>
        </div>
        
    </section>
      
<!--
<section class="section_three_home floor_p" id="amenities">
        <div class="content">
             <h2>Floor Plans</h2> 
            
            <ul class="menu_selections">
                                  <?php
//                Washington DC
                if( $post->post_parent == 324) {
        $args = array(
        'post_type' => 'floor_plans',
            'cat' => '10'
            
        );
                    } 
//                San Francisco
                else if( $post->post_parent == 326 ) {
                    
                $args = array(
        'post_type' => 'floor_plans',
                    'cat' => '9'
        ); 
                }
//                Atlanta
                else if( $post->post_parent == 320 ) {
                    
                $args = array(
        'post_type' => 'floor_plans',
                    'cat' => '8'
        ); 
                }
//                New York
                else {
                    
                $args = array(
        'post_type' => 'floor_plans',
                    'cat' => '7'
        ); 
                }
        $products = new WP_Query( $args );
            if( $products->have_posts() ) {
            while( $products->have_posts() ) {
        $products->the_post();
    ?> 
            
                <li>
                    <a href="#<?php the_field('post_id'); ?>">
                        <h5><?php the_field('floor'); ?></h5>
                    </a>
                </li>
    <?php
        }
            }
        else {
        echo 'No Floor Plans Found';
        }
    ?> 
   <?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>                  
            </ul>
        
        
        </div>
        
    </section>
-->

<section>
    <div class="our_services_section pricing" id="rates" style="padding-top:0;">
        <div class="wrapper">
            
            <section class="section_five_home" id="pricing">
                <div class="content">
                    
                                                      <?php
//                Washington DC
                if( $post->post_parent == 324) {
        $args = array(
        'post_type' => 'floor_plans',
            'cat' => '10'
            
        );
                    } 
//                San Francisco
                else if( $post->post_parent == 326 ) {
                    
                $args = array(
        'post_type' => 'floor_plans',
                    'cat' => '9'
        ); 
                }
//                Atlanta
                else if( $post->post_parent == 320 ) {
                    
                $args = array(
        'post_type' => 'floor_plans',
                    'cat' => '8'
        ); 
                }
//                New York
                else {
                    
                $args = array(
        'post_type' => 'floor_plans',
                    'cat' => '7'
        ); 
                }
        $products = new WP_Query( $args );
            if( $products->have_posts() ) {
            while( $products->have_posts() ) {
        $products->the_post();
    ?> 
                    <div class="package_overview floor_overview" id="<?php the_field('post_id'); ?>">
                        <h2><?php the_field('floor'); ?></h2>
                    <img src="<?php the_field('image'); ?>">
                        <?php the_field('chart'); ?>
                    </div>
            
    <?php
        }
            }
        else {
        echo 'No Floor Plans Found';
        }
    ?> 
   <?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>     


                </div>
            </section>
        </div>
    </div>
</section>
      
      



</section> 

<style>
    .site_holder header {
        position: relative;
    }
    .site_content {
        padding-top: 0
    }
</style>

<script>
  $(document).ready(function(){
      
      $(document).ready(function() {
        $('a[href*=\\#]').each(function(){
            $($(this)[0]).on('click', function(e){
                e.preventDefault();
                $('html, body').animate({
                    scrollTop : $(this.hash).offset().top - 150
                }, 500);
            });
        });
});
      
    $("#sticker").sticky({topSpacing:0});
  });
</script>

<?php
get_footer();
