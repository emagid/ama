<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header();
?>

    <div class="hero section section_default" id="splash">
        <div class="wrapper">
        <div class="hero_brand">
            <div class="wrapper">
        <div class="content">
            <p><?php the_title(); ?></p>
            <h2>Catch up on our latest research and insights.</h2>
            
        </div>
            </div>
        </div>
            
            <div class="two_sided">
                <div class="grey_side" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/grey_banner.png);">
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/mac_keyboard.png" style="top:50%;right:9%;">
                
                <div class="orange_side">
                    
                </div>
            </div>
        </div>
    
</div>

<section class="default_content">
        <div class="image_grid blog_grid">
            
<?php
$lastposts = get_posts( array(
    'posts_per_page' => 20
) );
 
if ( $lastposts ) {
    foreach ( $lastposts as $post ) :
        setup_postdata( $post ); ?>
              <a href="<?php the_permalink(); ?>">      
            <div class="project_grid blog_grid" style="background-image:url(<?php the_field('hero_image'); ?>)">
                
                    <div class="layer_oj">
                        <h4><?php the_title(); ?></h4>
                        <p><?php the_field('snippet'); ?></p>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png">
                    </div>
                
            </div>
        </a>
    
            
            
    <?php
    endforeach; 
    wp_reset_postdata();
}
   ?>  


            </div>
    
    <div class="sidebar_blog">
        <?php
get_sidebar();?>
    </div>
</section>


<?php
get_footer();
