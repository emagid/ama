<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header();
?>


<div class="hero section section_default" id="splash">
        <div class="wrapper">
        <div class="hero_brand">
            <div class="wrapper">
        <div class="content">
            <p>Featured Work</p>
            <h2>All the ways you love technology, we make them happen.</h2>
            
        </div>
            </div>
        </div>
            
            <div class="two_sided">
                <div class="grey_side" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/grey_banner.png);">
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/design_cards.png" style="right:9%;">
                
                <div class="orange_side">
                    
                </div>
            </div>
        </div>
    
</div>

<section class="default_content featured_work">
        <div class="image_grid">
      <?php
        $args = array(
        'post_type' => 'portfolio',
        'posts_per_page' => 99
        );
        $products = new WP_Query( $args );
            if( $products->have_posts() ) {
            while( $products->have_posts() ) {
        $products->the_post();
    ?> 
    

        <a href="<?php the_permalink(); ?>">
            <div class="project_grid" style="background-image:url(<?php the_field('hero_image'); ?>)">
                <div class="layer_oj" style="background:<?php the_field('project_color'); ?>;">
                    <h3><?php the_field('project'); ?></h3>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png">
                </div>

            </div>
        </a>
    
    

    <?php
        }
            }
        else {
        echo 'No Projects Found';
        }
    ?> 
            </div>
</section>

<section class="section_touch inner " style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/work.png)">
    <div class="content">
        <h4>Tell Us More About Your Project</h4>
        <div class="cta">
            <a href="/request-a-quote/">
                <button>Get Started <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></button>
            </a>
        </div>
    </div>

</section>


<?php
get_footer();
