<?php
/**
 * Template Name: Menus Template
 *
 */
get_header();
?>
  <section class="inner_template">     

<div class="nav_bar" id="sticker">
    <?php get_sidebar('location'); ?>
</div>



    <section class="section_three_home" id="amenities">
        <div class="content">
            <h2>Menus</h2>
            
            <ul class="menu_selections">
                                  <?php
//                Washington DC
                if( $post->post_parent == 324) {
        $args = array(
        'post_type' => 'menus',
            'cat' => '10'
            
        );
                    } 
//                San Francisco
                else if( $post->post_parent == 326 ) {
                    
                $args = array(
        'post_type' => 'menus',
                    'cat' => '9'
        ); 
                }
//                Atlanta
                else if( $post->post_parent == 320 ) {
                    
                $args = array(
        'post_type' => 'menus',
                    'cat' => '8'
        ); 
                }
//                New York
                else {
                    
                $args = array(
        'post_type' => 'menus',
                    'cat' => '7'
        ); 
                }
        $products = new WP_Query( $args );
            if( $products->have_posts() ) {
            while( $products->have_posts() ) {
        $products->the_post();
    ?> 
            
                <li>
                    <a href="#<?php the_field('post_id'); ?>">
                        <h5><?php the_field('name'); ?></h5>
                    </a>
                </li>
    <?php
        }
            }
        else {
        echo 'No Menus Found';
        }
    ?> 
   <?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>                  
            </ul>
        
        
        </div>
        
    </section>
    
      
<section>
    <div class="our_services_section pricing" id="rates">
        <div class="wrapper">
            
            <section class="section_five_home" id="hotels">
            <div class="content">

                                  <?php
//                Washington DC
                if( $post->post_parent == 324) {
        $args = array(
        'post_type' => 'menus',
            'cat' => '10'
            
        );
                    } 
//                San Francisco
                else if( $post->post_parent == 326 ) {
                    
                $args = array(
        'post_type' => 'menus',
                    'cat' => '9'
        ); 
                }
//                Atlanta
                else if( $post->post_parent == 320 ) {
                    
                $args = array(
        'post_type' => 'menus',
                    'cat' => '8'
        ); 
                }
//                New York
                else {
                    
                $args = array(
        'post_type' => 'menus',
                    'cat' => '7'
        ); 
                }
        $products = new WP_Query( $args );
            if( $products->have_posts() ) {
            while( $products->have_posts() ) {
        $products->the_post();
    ?> 
                <div class="media_box" id="menu">

                    <div class="header" id="<?php the_field('post_id'); ?>">
                        <div class="wrapper">
                            <h5><?php the_field('name'); ?></h5>
                            <p><?php the_field('note'); ?></p>
                        </div>
                    </div>     
                    <div class="price_chart">
                        <div class="iframe_map menu_bg" style="background-image:url(<?php the_field('image'); ?>)">
                            
                        </div>
                        <div class="hotel_info">
                            <?php the_field('menu'); ?>
                        </div> 
                    </div>
                </div>
    <?php
        }
            }
        else {
        echo 'No Menus Found';
        }
    ?> 
         
                


            
        </div>
                </section>
    </div>
    </div>
</section>

</section> 


<style>
    .site_holder header {
        position: relative;
    }
    .site_content {
        padding-top: 0
    }
</style>

<script>
$(document).ready(function(){

$(document).ready(function() {
    $('a[href*=\\#]').on('click', function(e){
        e.preventDefault();
        $('html, body').animate({
            scrollTop : $(this.hash).offset().top - 150
        }, 500);
    });
});
    
    $("#sticker").sticky({topSpacing:0});
    
    
});
</script>

<?php
get_footer();
