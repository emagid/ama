<?php
/**
 * Template Name: Content Template
 *
 */
get_header();
?>


    <section class="section_six_home" style="background-image:url(<?php the_field('banner_image'); ?>)">
        <div class="content header_template">
            
            <div class="get_started_header">
            <h4><?php the_title(); ?></h4>

            </div>
            
        </div>
    </section>

<section>
    <div class="our_services_section pricing" id="rates">
        <div class="wrapper">
            <section class="section_five_home" id="pricing">
                <div class="content">
                    <div class="package_overview">
                        <?php the_field('content'); ?>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>

<?php
get_footer();
