<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    
<!--    Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/fonts/fonts.css" rel="stylesheet">
    
    

    
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>
    
<!--    STICKY-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/libs/sticky/jquery.sticky.js"></script>
<!--    SWIPER-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/css/swiper.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/css/swiper.min.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/js/swiper.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/js/swiper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/js/swiper.esm.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/js/swiper.esm.bundle.js"></script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    

    
<div class="site_holder">

	<header>
        <div class="top_bar">
            <p class="home">Built for Meetings. Made for success. &nbsp; | &nbsp; <span>212-903-8277</span></p>
            <p class="ny" style="display:none;">
                Built for Meetings. Made for success. &nbsp; | &nbsp; <span>212-903-8060</span>
            </p>
            <p class="atl" style="display:none;">
                Built for Meetings. Made for success. &nbsp; | &nbsp; <span>404-892-7599</span>
            </p>
            <p class="sf" style="display:none;">
                Built for Meetings. Made for success. &nbsp; | &nbsp; <span>415-442-6770</span>
            </p>
            <p class="dc" style="display:none;">
                Built for Meetings. Made for success. &nbsp; | &nbsp; <span>571-481-2200</span>
            </p>
        </div>
        <div class="wrapper">
        
		<div class="site_branding">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="">
            </a>
		</div><!-- .site-branding -->

		<nav class="header_navigation">
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
			) );
			?>
		</nav><!-- #site-navigation -->
            
        </div> <!-- .wrapper -->
        
        <div class="window">
  <div class="header">
    <div class="burger-container">
      <div id="burger">
        <div class="bar topBar"></div>
        <div class="bar btmBar"></div>
      </div>

    </div>
              <div class="site_branding_mobile">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="">
            </a>
        </div>
    <div class="icon icon-apple"></div>
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-4',
				'menu_id'        => 'primary-menu',
			) );
			?>
    <div class="shop icon icon-bag"></div>
  </div>
</div>
	</header>

	<div class="site_content">
